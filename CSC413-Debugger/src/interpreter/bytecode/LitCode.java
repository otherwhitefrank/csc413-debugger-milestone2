package interpreter.bytecode;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * LIT ByteCode takes upto 2 args.
 * @author frank
 */
public class LitCode extends ByteCode {
    
    private int value;
    private String comment;
    private int numArgs;

    public LitCode() {
        super("LitCode");
    }
    /**
     * Store a literal on the top of the stack
     * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Push the value ontop of the stack
        vm.pushStack(this.value);
    
    }
    
    /**
     * Two forms of Debug printing 
     * for 1 arg == "LIT 0"
     * for 2 args == "LIT 0 m"
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "LIT 0" if no <id> is present
        //Otherwise print "LIT 0 <id>       int <id>"
        
        if (this.numArgs == 1)
        {
            //In case "LIT 0" simply print the original sourceByteCode
            return this.getSourceByteCode();
        }
        else
        {
            //Case "LIT 0 m      int m"
            String formattedString = this.getSourceByteCode() +
                    "\t\tint " + this.comment;
            return formattedString;
        }
    }

    @Override
    public void init(Vector<String> args) {
        if (args.size() == 1)
        {
            //Only one argument
            this.numArgs = 1;
            
            this.value = Integer.parseInt(args.get(0));
            
            this.comment = null;
        }
        else 
        {
            //Args must be 2
            this.numArgs = 2;
            
            this.value = Integer.parseInt(args.get(0));
            
            this.comment = args.get(1);
        }
    }
}
