package interpreter.bytecode;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * GOTO ByteCode, takes one argument
 * @author frank
 */
public class GotoCode extends ByteCode {
    
    private String jumpAddr; //Address to jump to

    public GotoCode() {
        super("GotoCode");
    }
    /**
     * Call label, retrieve address and tell VM to branch to address
     * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Tell the vm to branch to this address.
        vm.setProgramCounter(Integer.parseInt(this.jumpAddr));
    }
    
    /**
     * Standard Debug output
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "GOTO continue<<2>>"
        return this.getSourceByteCode();
    }

    public String getAddress()
    {
        return this.jumpAddr;
    }
    
    public void setAddress(String addr)
    {
        this.jumpAddr = addr;
    }
    
    @Override
    public void init(Vector<String> args) {        
        if (!args.isEmpty())
        {
            this.jumpAddr = args.get(0);
        }
    }
}
