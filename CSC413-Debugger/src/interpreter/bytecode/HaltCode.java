package interpreter.bytecode;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * HaltCode class that implements the abstract class ByteCode, takes 0 arguments.
 * @author Frank Dye
 */
public class HaltCode extends ByteCode {

    public HaltCode() {
        super("HaltCode");
    }

    /**
     * Tell VM to halt execution of the program.
     * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Tell VM to halt-execution
        vm.haltExecution();
    }
    
    /**
     * Standard Debug output
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "HALT"
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {
        //No arguments to process for HALT
    }
}

