package interpreter.debugger;

import java.util.Iterator;
import java.util.Set;
import lexer.Symbol;
import lexer.Tokens;

class Binder {

    /**
     * <
     * pre>
     *  Binder objects group 3 fields
     *  1. a value
     *  2. the next link in the chain of symbols in the current scope
     *  3. the next link of a previous Binder for the same identifier
     *     in a previous scope
     * </pre>
     */

    private Object value;
    private String prevtop;   // prior symbol in same scope
    private Binder tail;      // prior binder for same symbol
    // restore this when closing scope

    Binder(Object v, String p, Binder t) {
        value = v;
        prevtop = p;
        tail = t;
    }

    Object getValue() {
        return value;
    }

    String getPrevtop() {
        return prevtop;
    }

    Binder getTail() {
        return tail;
    }
}

/**
 *
 * @author frank
 */
/**
 * <
 * pre>
 * The SymbolTable class is similar to java.util.Dictionary, except that
 * each key must be a Symbol and there is a scope mechanism.
 *
 * Consider the following sequence of events for table t:
 * t.put(Symbol("a"),5)
 * t.beginScope()
 * t.put(Symbol("b"),7)
 * t.put(Symbol("a"),9)
 *
 * symbols will have the key/value pairs for Symbols "a" and "b" as:
 *
 * Symbol("a") ->
 *     Binder(9, Symbol("b") , Binder(5, null, null) )
 * (the second field has a reference to the prior Symbol added in this
 * scope; the third field refers to the Binder for the Symbol("a")
 * included in the prior scope)
 * Binder has 2 linked lists - the second field contains list of symbols
 * added to the current scope; the third field contains the list of
 * Binders for the Symbols with the same string id - in this case, "a"
 *
 * Symbol("b") ->
 *     Binder(7, null, null)
 * (the second field is null since there are no other symbols to link
 * in this scope; the third field is null since there is no Symbol("b")
 * in prior scopes)
 *
 * top has a reference to Symbol("a") which was the last symbol added
 * to current scope
 *
 * Note: What happens if a symbol is defined twice in the same scope??
 * </pre>
 */
public class SymbolTable {

    private java.util.HashMap<String, Binder> symbols = new java.util.HashMap<String, Binder>();
    private java.util.Stack<String> prevKeys = new java.util.Stack<String>();
    private String top;    // reference to last symbol added to
    // current scope; this essentially is the
    // start of a linked list of symbols in scope
    private Binder marks;  // scope mark; essentially we have a stack of
    // marks - push for new scope; pop when closing
    // scope

    /*
     public static void main(String args[]) {
     Symbol s = Symbol.symbol("a", 1),
     s1 = Symbol.symbol("b", 2),
     s2 = Symbol.symbol("c", 3);

     Table t = new Table();
     t.beginScope();
     t.put(s,"top-level a");
     t.put(s1,"top-level b");
     t.beginScope();
     t.put(s2,"second-level c");
     t.put(s,"second-level a");
     t.endScope();
     t.put(s2,"top-level c");
     t.endScope();
     }

     */
    public SymbolTable() {
    }

    /**
     * <pre>Overloaded output toString to return a formatted string of the key/val pairs.
     * @return formatted output string. 
     */
    @Override
    public String toString() {
        String returnVal = "";
        Set<String> keys = symbols.keySet();
        Iterator<String> iterator = keys.iterator();

        //Get all key/value pairs in the symTable 
        while (iterator.hasNext())
        {
            String element = iterator.next();
            Binder bind = symbols.get(element);
            returnVal += element + "/" + bind.getValue();
            if (iterator.hasNext())
            {
                returnVal += ",";
            }
        }
        
        //Return the formatted string
        return returnVal;
    }

    /**
     * Gets the object associated with the specified symbol in the Table.
     */
    public Object get(Symbol key) {
        Binder e = symbols.get(key);
        return e.getValue();
    }

    /**
     * Puts the specified value into the Table, bound to the specified
     * Symbol.<br>
     * Maintain the list of symbols in the current scope (top);<br>
     * Add to list of symbols in prior scope with the same string identifier
     */
    public void put(String key, Object value) {
        symbols.put(key, new Binder(value, top, symbols.get(key)));
        prevKeys.push(key); //Save the key so we can later pop them out
        top = key;
    }

    /**
     * Remembers the current state of the Table; push new mark on mark stack
     */
    public void beginScope() {
        marks = new Binder(null, top, marks);
        top = null;
    }

    /**
     * Restores the table to what it was at the most recent beginScope that has
     * not already been ended.
     */
    public void endScope() {
        while (top != null)
        {
            Binder e = symbols.get(top);
            if (e.getTail() != null)
            {
                symbols.put(top, e.getTail());
            }
            else
            {
                symbols.remove(top);
            }
            top = e.getPrevtop();
        }
        top = marks.getPrevtop();
        marks = marks.getTail();
    }

    public void pop(int n) {
        //Pop n items off our prevKey stack and remove them from the symTable as well.
        for (int i = 0; i < n; i++)
        {
            //Retrieve the last key put in the symbol table
            String key = prevKeys.pop();
            Binder bind = symbols.get(key);
            if (bind.getTail() != null)
            {
                //We have previous values for this key pair, so restore them.
                Binder oldBind = bind.getTail();
                symbols.remove(key);
                symbols.put(key, oldBind); //Restore old binder
            }
            else
            {
                //No previous values, just remove the key pair
                symbols.remove(key);
            }
        }
    }

    /**
     * @return a set of the Table's symbols.
     */
    public java.util.Set<String> keys() {
        return symbols.keySet();
    }
}
